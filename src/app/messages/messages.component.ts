import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';
import { Recette } from '../recettes';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  test: Recette;
  constructor(public messageService: MessageService) {}

  ngOnInit() {
  }

}
