import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { RecetteService } from '../recette.service';
import { Recette } from '../recettes';

@Component({
  selector: 'app-modif-recette',
  templateUrl: './modif-recette.component.html',
  styleUrls: ['./modif-recette.component.css']
})
export class ModifRecetteComponent implements OnInit {
  selectedRecette: Recette;
  constructor(
    private route: ActivatedRoute,
    private recetteService: RecetteService,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.getRecette();
  }

  getRecette(): void {
    const id = this.route.snapshot.paramMap.get('_id');
    console.log(id);
    this.recetteService.getRecettebyId(id).subscribe((recette) => {
      this.selectedRecette = recette;
      console.log(this.selectedRecette[0]);
    });
  }
  updateRecette(): void {
    this.recetteService.updateRecette(this.selectedRecette[0]).
    subscribe(() => this.getRecette());
    this.getRecette();
    this.goBack();

  }
  goBack(): void {
    this.location.back();
  }
  deleteIngredient(): void {
    console.log(this.selectedRecette[0].ingredients);
    this.selectedRecette[0].ingredients.pop();
  }
  addIngredient(): void {
    this.selectedRecette[0].ingredients.push({ name: '', weight: 0 });
  }

}
