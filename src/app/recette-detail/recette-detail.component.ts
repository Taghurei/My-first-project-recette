import { Component, OnInit, Input } from '@angular/core';
import { Recette } from '../recettes';
import { RecetteService } from '../recette.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-recette-detail',
  templateUrl: './recette-detail.component.html',
  styleUrls: ['./recette-detail.component.scss']
})
// gerer le routing jusqu'ici - marche sans

export class RecetteDetailComponent implements OnInit {
  constructor(private recetteService: RecetteService,
      private route: ActivatedRoute,
    private location: Location) { }
recette: Recette;

  ngOnInit() {
    this.getRecette();
  }
  goBack(): void {
    this.location.back();
  }

  getRecette(): void {
    const id = this.route.snapshot.paramMap.get('_id');
    console.log(id);
    this.recetteService.getRecettebyId(id).subscribe((recette) => {
      this.recette = recette;
      console.log(this.recette[0]);
    });
  }
  deleteRecette(): void {
    this.recetteService.deleteRecette(this.recette[0]).
      subscribe(() => (this.getRecette(),     this.goBack()) );
  }
/*modifierRecette() {
    this.detailbool = true;
    this.modifbool = true;
  }
  masquerDetail() {
    this.detailbool = true;
    this.modifbool = false;
  }
  updateRecette(): void {
    this.recetteService.updateRecette(this.recette).
      subscribe(() => this.getRecette());
    this.detailbool = false;
    this.modifbool = false;
    this.getRecette();
  }
  deleteIngredient(): void {
    this.recette.ingredients.pop();
  }
  addIngredient(): void {
    this.recette.ingredients.push({ name: '', weight: 0 });
  }
}
*/
}
