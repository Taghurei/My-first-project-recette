import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddRecetteComponent } from './add-recette/add-recette.component';
import { RecetteComponent } from './recette/recette.component';
import { RecetteDetailComponent } from './recette-detail/recette-detail.component';
import { MessagesComponent } from './messages/messages.component';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { LoginComponent } from './login/login.component';
import { ModifRecetteComponent } from './modif-recette/modif-recette.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  { path: 'add-recettes', component: AddRecetteComponent },
  { path: 'recettes', component: RecetteComponent},
  { path: 'recettes/details/:_id', component: RecetteDetailComponent },
  { path: 'recettes/details/:_id/change', canActivate: [AuthGuard], component: ModifRecetteComponent},

  {
    path: '',
    redirectTo: '/recettes', pathMatch: 'full'
  },
  {
    path: 'recettes/search/_id/:_id', component: ModifRecetteComponent
  },
  { path: 'login', component: LoginComponent },
  {path: 'register', component: RegisterComponent}



];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
