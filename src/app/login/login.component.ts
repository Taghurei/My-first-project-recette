import { Component, OnInit, Input } from '@angular/core';
import { User } from '../users';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { UserService } from '../users.service';
import { AuthService } from '../auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: User = { 'email': '', 'password': '', 'token': 'token' };
  constructor(private userService: UserService,
    private location: Location,
    private route: ActivatedRoute,
    private authService: AuthService) { }

  ngOnInit() {


  }

  goBack(): void {
    this.location.back();
  }
  create(): void {
    console.log('connexion tentative');
    this.userService.createUser(this.user).
      subscribe();
  }
  login(): void {
    console.log('connexion tentative');
    this.userService.connect(this.user).
      subscribe(user => { this.user.token = user.user.token, this.authService.setToken(user.user.token); });
      this.goBack();

  }
  current(): void {
    console.log('connexion tentative');
    this.userService.getCurrent().
      subscribe();
  }
}
