import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Location } from '@angular/common';
import { FilterPipe } from './filter.pipe';
import { AppComponent } from './app.component';
import { RecetteComponent } from './recette/recette.component';
import { RecetteDetailComponent } from './recette-detail/recette-detail.component';
import { AddRecetteComponent } from './add-recette/add-recette.component';
import { MessagesComponent } from './messages/messages.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './/app-routing.module';
import { AuthRequestOptions } from './auth-request';
import { RequestOptions } from '@angular/http';
import { AuthErrorHandler } from './auth-error-handler';
import { LoginComponent } from './login/login.component';
import { ModifRecetteComponent } from './modif-recette/modif-recette.component';
import { RegisterComponent } from './register/register.component';
import {AuthGuard} from './auth.guard';
import {AuthService} from './auth.service';
import { HttpModule } from '@angular/http';


@NgModule({
  declarations: [AppComponent,
    RecetteComponent,
    RecetteDetailComponent,
    AddRecetteComponent,
    MessagesComponent,
    FilterPipe,
    LoginComponent,
    ModifRecetteComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    HttpModule
  ],
  providers: [
    AuthGuard,
    AuthService,
    {
      provide: RequestOptions,
      useClass: AuthRequestOptions
    },
    {
      provide: ErrorHandler,
      useClass: AuthErrorHandler
    }


  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
