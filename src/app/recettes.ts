import {Ingredient} from './ingredients';

export class Recette {
    name: string;
    country: string;
    ingredients: Ingredient[];
    difficulty: string;
    explication: string;
    type: string;
    url: string;
    _id: string;
}
