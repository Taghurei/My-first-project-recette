import { Component, OnInit } from '@angular/core';
import { User } from '../users';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { UserService } from '../users.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user: User = { 'email': '', 'password': '', 'token': 'token' };
  confirm: String;
  confirmationbool: boolean;
  constructor(private userService: UserService,
    private location: Location,
    private route: ActivatedRoute,
    private authService: AuthService) { }

  ngOnInit() {
  }
  goBack(): void {
    this.location.back();
  }
  create(): void {
    console.log('connexion tentative');
    if (this.confirm === this.user.password) {
      this.userService.createUser(this.user).
        subscribe();
      this.goBack();
      this.goBack();

    } else {
      this.confirmationbool = true;
    }
  }
}
