import { Injectable } from '@angular/core';
import { User } from './users';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Headers } from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userUrl = ' http://localhost:3000/users/';
  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }
  private urlWithId;
  user: User;
  headers: Headers;
  connect(user: User): Observable<any> {
    console.log({ user });
    this.user = user;
    console.log( this.user );

    return this.http.post(this.userUrl + 'login', { user });
  }

  getCurrent(): Observable<User> {

    return this.http.get<User>(this.userUrl + 'current', { headers: { 'Authorization': 'Token ' + this.user.token } });
  }
  createUser(user: User): Observable<any> {
    console.log({ user });
    console.log(this.headers);
    return this.http.post(this.userUrl, { user }, );
  }

}
