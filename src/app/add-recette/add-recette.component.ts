import { Component, OnInit, Input } from '@angular/core';
import { Recette } from '../recettes';
import { RecetteService } from '../recette.service';
import { SharedService } from '../shared.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-recette',
  templateUrl: './add-recette.component.html',
  styleUrls: ['./add-recette.component.css']
})
export class AddRecetteComponent implements OnInit {
  newRecipe: Recette;
  title: string;
  recettes: Recette[];

  @Input() addRecipebool: boolean;
  newRecette: Recette;

  validateRecette: boolean;
  constructor(private recetteService: RecetteService,
    private location: Location,
    private route: ActivatedRoute,

  ) {
  }

  ngOnInit() {
    this.getRecette(),
    this.newRecette = new Recette();
    this.newRecette.country = '',
    this.newRecette.name = '',
    this.newRecette.difficulty = '',
    this.newRecette.type = '',
    this.newRecette.url = '',
    this.newRecette.ingredients = [{ 'name': '', 'weight': 0 }, { 'name': '', 'weight': 0 }];


  }
  getRecette(): void {
    this.recetteService.getRecette().subscribe(recettes => this.recettes = recettes);
  }
  addRecette(): void {
    this.recetteService.addRecette(this.newRecette).
      subscribe(() => this.getRecette());
    this.goBack();
  }
  goBack(): void {
    this.location.back();
  }
  deleteIngredient(): void {
    this.newRecette.ingredients.pop();
  }
  addIngredient(): void {
    this.newRecette.ingredients.push({ name: '', weight: 0 });
  }
}
