import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    public router: Router,
    public authService: AuthService) { }

  canActivate() {
    if (!this.authService.isTokenExpired()) {
      console.log('authguardtrue');
      return true;


    }
    console.log('authguardfalse');
    this.router.navigate(['/login']);

    return false;
  }

}
