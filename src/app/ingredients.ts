
export class Ingredient {
    name: string;
    weight: number;
}
