import { Injectable } from '@angular/core';
import { Recette } from './recettes';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RecetteService {
  private recetteUrl = ' http://localhost:3000/recettes';
  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }
  private urlWithId;
  selectedRecette: Recette;
  getRecette(): Observable<Recette[]> {
    return this.http.get<Recette[]>(this.recetteUrl);
  }
  addRecette(newRecette: Recette): Observable<any> {
    console.log(newRecette);
    return this.http.post(this.recetteUrl, newRecette);
  }
  // Recherche par clé non-primaire => array
  getRecettebyId(id: string): Observable<Recette> {
    return this.http.get<Recette>(this.recetteUrl + '/search/_id/' + id);
  }

  searchRecette(search: String): Observable<Recette[]> {

    return this.http.get<Recette[]>(this.recetteUrl + '/search/' + search);
  }
  deleteRecette(newRecette: Recette): Observable<any> {
    return this.http.delete(this.recetteUrl + '/search/name/' + newRecette.name);
  }
  updateRecette(newRecette: Recette): Observable<any> {
    console.log('url:' + this.recetteUrl + '/' + newRecette._id);
    return this.http.put(this.recetteUrl + '/' + newRecette._id, newRecette);
  }
  private log(message: string) {
    this.messageService.add('RecetteService: ' + message);
  }
}
