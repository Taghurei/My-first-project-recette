import { Component, OnInit, Output } from '@angular/core';
import { Recette } from '../recettes';
import { RecetteService } from '../recette.service';

import { AuthService } from '../auth.service';


@Component({
  selector: 'app-recette',
  templateUrl: './recette.component.html',
  styleUrls: ['./recette.component.scss']
})
export class RecetteComponent implements OnInit {
  recettes: Recette[];
  test = ['1', '2'];
  constructor(private recetteService: RecetteService, private authService: AuthService) { }

  recherche: String;
  @Output() selectedRecette: Recette;
  @Output() showExplanation: String;
  @Output() addRecette: boolean;
  @Output() detailbool: boolean;
  searchbool: boolean;
  presearchbool: boolean;
  deconnectbool: boolean;
  getRecette(): void {
    this.recetteService.getRecette().subscribe(recettes => this.recettes = recettes);
    this.searchbool = false;
  }

  onSelect(recette: Recette): void {
    this.selectedRecette = recette;
    this.detailbool = false;
    console.log(this.selectedRecette.name);


  }
  selection(recherche) {
    this.searchbool = false;
    this.presearchbool = false;
    console.log('recherche');
    this.recetteService.searchRecette(recherche).subscribe(recettes => this.recettes = recettes);

  }
  preselection(recherche) {
    if (recherche === '') {
      this.recetteService.getRecette().subscribe(recettes => this.recettes = recettes);
      this.presearchbool = false;
    } else {
      this.presearchbool = true;
      this.recetteService.searchRecette(recherche).subscribe(recettes => this.recettes = recettes);
    }
  }

  deconnexion(): void {
    this.authService.setToken('');
  }
  addRecipe(): void {
    this.addRecette = true;
  }
  getConnect() {
    if (this.authService.getToken() === '') {
      this.deconnectbool = true;
    } else {
      this.deconnectbool = false;

    }
  }
  ngOnInit() {
    this.getRecette();
    this.getConnect();

  }

}
