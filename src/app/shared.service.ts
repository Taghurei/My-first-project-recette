import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
 providedIn: 'root'
})
export class SharedService {
 private emitChangeSource = new Subject<any>();
 changeEmitted$ = this.emitChangeSource.asObservable();
 emitChange(change: any) {
   this.emitChangeSource.next(change);
 }
}
